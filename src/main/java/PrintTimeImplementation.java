import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PrintTimeImplementation implements PrintCurrentTime {

    public String print() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}

