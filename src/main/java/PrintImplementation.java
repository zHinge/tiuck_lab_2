import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PrintImplementation implements PrintCurrentTime {
    public String print() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }
}
