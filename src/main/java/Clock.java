import java.time.Instant;

public class Clock implements PrintCurrentTime {

    public String print() {
        Instant instant = Instant.now();
        return instant.toString();
    }
}
