import java.sql.Timestamp;

public class TimePrinter implements PrintCurrentTime {
    public String print(){
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(currentTimestamp);
        return currentTimestamp.toString();
    }
}
